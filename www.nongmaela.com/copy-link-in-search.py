import os
import requests

def download_images(url_array, base_folder_name):
  """Downloads images from a URL array and creates folders/subfolders."""
  for url in url_array:
    # Extract filename and folder structure
    filename = url.split("/")[-1]  # Get the filename from the URL

    # Remove "https://" from URL before splitting
    url_without_protocol = url[8:]  # Slice the string to remove "https://"
    folder_path = os.path.join(base_folder_name, *url_without_protocol.split("/")[:-1])

    # Create folders if they don't exist
    os.makedirs(folder_path, exist_ok=True)  # Create folders (and parents) if needed

    # Download image
    response = requests.get(url, stream=True)
    if response.status_code == 200:
      with open(os.path.join(folder_path, filename), "wb") as f:
        for chunk in response.iter_content(1024):
          f.write(chunk)
      print(f"Downloaded {filename} to {folder_path}")
    else:
      print(f"Failed to download {url} (status code: {response.status_code})")

# Example usage with your URL array
url_array = [
  "../7d5ae077a2874a30a5b4fc398d197/036/files/../7d5ae077a2874a30a5b4fc398d197/036_1.min.css",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/../7d5ae077a2874a30a5b4fc398d197/036_home_1.min.css",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Calendar_2021_GE_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Calendar_2021_GT_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Reference_Form_GE_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Reference_Form_GT_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Student_Covenant_GE_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Student_Covenant_GT_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Student_Form_GE_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Student_Form_GT_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Student_Handbook_GE_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/Student_Handbook_GT_small.pdf",
  "../7d5ae077a2874a30a5b4fc398d197/036/site_favicon_16_1630915830081.ico",
  "../fonts/s/amaranth/v18/KtkpALODe433f0j1zMF-OMWl42E.woff2",
  "../fonts/s/amaranth/v18/KtkuALODe433f0j1zMnFHdA.woff2",
  "../fonts/s/cabin/v27/u-4i0qWljRw-PfU81xCKCpdpbgZJl6Xvp9nsBXw.woff2",
  "../fonts/s/cabin/v27/u-4i0qWljRw-PfU81xCKCpdpbgZJl6XvptnsBXw.woff2",
  "../fonts/s/cabin/v27/u-4i0qWljRw-PfU81xCKCpdpbgZJl6Xvqdns.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS-muw.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS2mu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSCmu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSGmu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSKmu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSOmu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSumu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSymu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTUGmu1aB.woff2",
  "../fonts/s/opensans/v40/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTVOmu1aB.woff2",
  "../fonts/s/sourcesanspro/v22/6xK3dSBYKcSV-LCoeQqfX1RYOo3qN67lqDY.woff2",
  "../fonts/s/sourcesanspro/v22/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNa7lqDY.woff2",
  "../fonts/s/sourcesanspro/v22/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNK7lqDY.woff2",
  "../fonts/s/sourcesanspro/v22/6xK3dSBYKcSV-LCoeQqfX1RYOo3qNq7lqDY.woff2",
  "../fonts/s/sourcesanspro/v22/6xK3dSBYKcSV-LCoeQqfX1RYOo3qO67lqDY.woff2",
  "../fonts/s/sourcesanspro/v22/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7l.woff2",
  "../fonts/s/sourcesanspro/v22/6xK3dSBYKcSV-LCoeQqfX1RYOo3qPK7lqDY.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwkxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwmBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwmhduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwmRduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwmxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i94_wkxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i94_wlBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i94_wlxdu.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i94_wmBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i94_wmhduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i94_wmRduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3i94_wmxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwkxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmhduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmRduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwmxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwkxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlxdu.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmhduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmRduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwmxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3iu4nwkxduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3iu4nwlBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3iu4nwlxdu.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3iu4nwmBduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3iu4nwmhduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3iu4nwmRduz8A.woff2",
  "../fonts/s/sourcesanspro/v22/6xKydSBYKcSV-LCoeQqfX1RYOo3iu4nwmxduz8A.woff2",
  "../WIDGET_CSS/1b33305381e89e7968d1bedf3d577f17.css",
  "../WIDGET_CSS/44bbfeeb55cb5b35ac69c2822f94bb21.css",
  "../WIDGET_CSS/5f89d8406055ed4bb4b2143fa4df780c.css",
  "../WIDGET_CSS/8ddc5e3fe4ec66ede76d2d2c4c25d233.css",
  "../WIDGET_CSS/9bc88bd960403e330aa81dee6ab8ae50.css",
  "../WIDGET_CSS/bc28792ec471c843b89b3776a26d8255.css",
  "../WIDGET_CSS/d3852b16f25e7b60ca6046bb1d47e468.css",
  "../WIDGET_CSS/f874515d9238143370d5405280dd0f9b.css",
  "../_dm/s/rt/scripts/vendor/photoSwipe/icons@2x.png",
  "../_dm/s/rt/scripts/vendor/photoswipe4/icons/default-skin.png",
  "../_dm/s/rt/scripts/vendor/photoswipe4/icons/default-skin.svg",
  "../_dm/s/rt/scripts/vendor/photoswipe4/icons/preloader.gif\n\nwhats-new-new.html:",
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Flower+cross+sign.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Trimming+around+the+lights.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Painting+outside+back+hong+prachum.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Paint+crew+finished.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/girls+post.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Covering+the+ball+marks.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Girl+paints+post.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Fern.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Suchaat.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/hong+prachum+painted.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Pecan+grove.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Table+skirts+close.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/IMG20210817104530.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Table+skirts.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/IMG20210817104540.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Creating+front+porch+stair.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Front+porch+stair+finished.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/First+step.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/rafter+beams+ready.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Ready+to+pour+concrete.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Progress+angled.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/wet+concrete.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Skeletal+roofing.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Adding+roofing.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Back+porch+progress.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Finished.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Empty+shelves+vert.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Jane+building+shelves.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Out+the+door.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Shelves+with+stuff+horizontal.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Curtains+4.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Fan+and+map+on+wall.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Curtains+three+corner+right.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Students+Frames+cropped.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Curtains+closed.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Big+Screen+TV.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bull+board+fan+on+wall.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/knock+it+down.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/flattened.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Razed.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/digging+foundation.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Rebar+Posts.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Concrete+Line.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Footers.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Step4.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks+2.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks+4a.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks5.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks+back.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/half+chopped.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Chopped.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Roofing.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Awning.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Sidewalk2.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Inside+finishing.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BackAngle+right.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Windows.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Library+inside.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Inside.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Done.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Night2.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/New+Sign+Pine2+medium.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Hal+Bouganvilla.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Plants+moved+copy.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Sign+smallest+reflestion+copy.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Web+RGB+Small-Black+100x438+72+reso+copy.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+2.svg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+2.svg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email.svg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Manop+creating+sign.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Manop+Guitar.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BT7.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BT+wine2.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/hal.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/teacher-thai-on-chalkboard.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Vertical+Classroom+Best.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Hal+animated-df974011.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/May1.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Wichan+BT.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Registration.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/khiaw.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/leaders.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/soccer1.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/soccer2.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/soccer3.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/May+%26+Geek.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Boon+Reun.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/girls+slingshot.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Audience.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/In+the+grass.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Emmon.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/shake+hands.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/RakThai.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/leading+singing.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Tire+roll.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/rung%2Cbubay%2C+bay.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/peace+man.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/old+man.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BBQ.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Sadudee.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/a+class+of+friends.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/praying+together.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/harvest+skit.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Jacob.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Joe+Slingshot.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/IMG_0601.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/table+talk.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/IMG_0539.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Group+shot.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Martin.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Thong+Dii.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Tat.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Ice+Cream.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Balloon1.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Balloon2.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Balloon3.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Karaoke.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Faa.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BT+Rakthai.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/2+Girls.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Building+Dedication+Cropped.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Kay.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Kids.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Current+Students.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Reunion.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Dom.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Listening.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Hi.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Building+Group.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Singing.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Table.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Pictures.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Water+Relay.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Girl+Running.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Kon.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Old+Couple.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Water+Over+Head.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Drums.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/SPLASH.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Group+Shot+20-d9041051.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Hal.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Wat1.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Not+Face.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Phoom+and+wife+1.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Singing.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/parade.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Manop.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BT.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Suke+group-bedda50a.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Ailyn+and+MOm.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Chaat1.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/may-+Phat.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Noy+group+2.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Grad+Group+Heart.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Emmon+-+Ailyn.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Piano.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Grad+Group2.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/4+boys.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Boon+Lo.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Entrance+BT.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Students+in+a+line.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Na+Smiling.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Tak+and+Fam.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Kay+Fam.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Opening+Prayer.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Dom.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Grads+and+Teachers.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/charlie.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/butter.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Som+Testimony.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Johan+Group.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Girls.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BT+Grad+Night.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/May+and+UTe.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Grads+Singing1.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Being+prayed+for1.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-00+Russel+in+classroom.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-99+Manop+in+wig.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-99+Long+and+family.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Five+Gilrs+in+Tribal+outfits.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Faa+Painting+2006.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-01+Ant+Eggs.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Don+and+Janet.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/PonJon+Sewing.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BT+8-06.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-01+kon.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Jane+and+Duan.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Six+with+Guitar+2002.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Faa+and+Friend+Graduation+10-06.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Don+Graduation+2000a.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Musicians+2001.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-00+Serving+Takraw.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/chaay+posing+2002.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Mom+at+a+loom+2001.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Game+Night.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Dee+and+Shy+Family.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-01+sawing+log.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-02+Mote+Weaving.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/singing+in+church+2001.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/boy+on+fallen+tree+reading2-06.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-01+2+Friends+%28girls%29.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-01+Girl+cutting+bamboo+into+thin+strips.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/-99+Amnaat+opening+coconut.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/files/uploaded/2020-2021_Calendar.pdf',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/starting+cooking+fire.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Nii+cooking.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/cooking+pot-e584a10f.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Crushing+garlic.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/First+House+copy.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Student+Front+Porch.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Girls+Dorm+copy.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/inside+Room+copy.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Ditch.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Am+work+detail.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Mower2.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Sawing+tree.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Suke+singing+guitar+2.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Nooy+Bell3+best+copy.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Pat+game2.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Slingshot1.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/IMG_8046.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/LeftQuote.svg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Phrae+Face+Shot.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/LeftQuote.svg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Wang.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/LeftQuote.svg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Pong-bd9437d0.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/LeftQuote.svg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Flower+cross+sign.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Trimming+around+the+lights.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Painting+outside+back+hong+prachum.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Paint+crew+finished.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/girls+post.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Covering+the+ball+marks.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Girl+paints+post.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Fern.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Suchaat.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/hong+prachum+painted.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Pecan+grove.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Table+skirts+close.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/IMG20210817104530.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Table+skirts.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/IMG20210817104540.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Creating+front+porch+stair.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Front+porch+stair+finished.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/First+step.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/rafter+beams+ready.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Ready+to+pour+concrete.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Progress+angled.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/wet+concrete.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Skeletal+roofing.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Adding+roofing.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Back+porch+progress.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Finished.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Empty+shelves+vert.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Jane+building+shelves.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Out+the+door.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Shelves+with+stuff+horizontal.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Curtains+4.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Fan+and+map+on+wall.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Curtains+three+corner+right.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Students+Frames+cropped.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Curtains+closed.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Big+Screen+TV.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bull+board+fan+on+wall.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/knock+it+down.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/flattened.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Razed.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/digging+foundation.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Rebar+Posts.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Concrete+Line.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Footers.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Step4.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks+2.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks+4a.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks5.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Bricks+back.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/half+chopped.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Chopped.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Roofing.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Awning.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Sidewalk2.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Inside+finishing.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/BackAngle+right.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Windows.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Library+inside.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Inside.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Done.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Night2.jpg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/New+Sign+Pine2+medium.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Hal+Bouganvilla.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Plants+moved+copy.JPg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Sign+smallest+reflestion+copy.jpeg',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/Black+Thai+logo.png',
  '../7d5ae077a2874a30a5b4fc398d197/036/dms3rep/multi/NML+email+clipped.png',
];

base_folder_name = "downloaded_images"

download_images(url_array, base_folder_name)
